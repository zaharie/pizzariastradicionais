<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class CreateUsersTable extends Migration
{
/**
* Run the migrations.
*
* @return void
*/
public function up()
{
Schema::create('users', function (Blueprint $table) {
$table->increments('id');
$table->string('name');
$table->string('email')->unique();
$table->string('password');
$table->string('server_url');
$table->string('cep');
$table->string('phone');
$table->string('waiting_time')->nullable();
$table->string('cnpj');
$table->rememberToken();
$table->timestamps();
});
Schema::create('border', function (Blueprint $table) {
$table->integer('id_user')->unsigned();
$table->foreign('id_user')->references('id')->on('users');
$table->increments('id');
$table->string('name');
$table->boolean('active');
$table->timestamps();
});
Schema::create('pizza_size', function (Blueprint $table) {
$table->increments('id');
$table->integer('id_user')->unsigned();
$table->foreign('id_user')->references('id')->on('users');
$table->string('name');
$table->double('price');
$table->double('price_border');
$table->double('price_promotional');
$table->boolean('promotion')->nullable();
$table->integer('quantity_pieces');
$table->integer('quantity_flavors');
$table->boolean('active');
$table->timestamps();
});
Schema::create('pizza', function (Blueprint $table) {

    $table->string('obs')->nullable();
$table->increments('id');
$table->integer('id_user')->unsigned();
$table->foreign('id_user')->references('id')->on('users');
$table->integer('id_size')->unsigned();
$table->foreign('id_size')->references('id')->on('pizza_size');
$table->integer('border')->unsigned()->nullable();
$table->foreign('border')->references('id')->on('border');
$table->timestamps();
});
Schema::create('drink', function (Blueprint $table) {
$table->increments('id');
$table->integer('id_user')->unsigned();
$table->foreign('id_user')->references('id')->on('users');
$table->string('name');
$table->double('price');
$table->boolean('promotion')->nullable();
$table->double('price_promotional');
$table->boolean('active');
$table->timestamps();
});
    Schema::create('category', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('id_user')->unsigned();
        $table->foreign('id_user')->references('id')->on('users');
        $table->string('name');
        $table->boolean('active');
        $table->timestamps();
    });
Schema::create('flavor', function (Blueprint $table) {
$table->increments('id');
$table->integer('id_user')->unsigned();
$table->foreign('id_user')->references('id')->on('users');
$table->string('name');
$table->string('type');
$table->double('price');
$table->double('price_promotional');
$table->boolean('promotion')->nullable();
$table->boolean('active');
$table->timestamps();
});
    Schema::create('flavor_category', function (Blueprint $table) {
        $table->integer('id_user')->unsigned();
        $table->foreign('id_user')->references('id')->on('users');
        $table->integer('id_flavor')->unsigned();
        $table->foreign('id_flavor')->references('id')->on('flavor');
        $table->integer('id_category')->unsigned();
        $table->foreign('id_category')->references('id')->on('category');
        $table->timestamps();
    });
Schema::create('flavor_pizza', function (Blueprint $table) {
$table->increments('id');
$table->integer('id_pizza')->unsigned();
$table->foreign('id_pizza')->references('id')->on('pizza');
$table->integer('id_flavor')->unsigned();
$table->foreign('id_flavor')->references('id')->on('flavor');
$table->timestamps();
});
Schema::create('image', function (Blueprint $table) {
$table->increments('id');
$table->string('image');
$table->integer('id_flavor')->unsigned()->nullable();
$table->foreign('id_flavor')->references('id')->on('flavor');
$table->timestamps();
});
Schema::create('housing', function (Blueprint $table) {
    $table->increments('id');
$table->boolean('active');
$table->string('name');
$table->double('tax');
$table->integer('id_user')->unsigned();
$table->foreign('id_user')->references('id')->on('users');
$table->timestamps();
});
        Schema::create('client', function (Blueprint $table) {
            $table->integer('id_user')->unsigned();
            $table->foreign('id_user')->references('id')->on('users');
        $table->increments('id');
        $table->string('name');
        $table->string('last_name');

        $table->string('profile_pic')->nullable();
        $table->string('id_face')->unique()->unsigned();

        $table->boolean('status');
        $table->string('reason')->nullable();
        $table->timestamps();
    });
Schema::create('delivery_cost', function (Blueprint $table) {
    $table->integer('id_user')->unsigned();
    $table->foreign('id_user')->references('id')->on('users');
    $table->increments('id');
    $table->double('tax');
    $table->double('tax_min');
});

Schema::create('order', function (Blueprint $table) {
$table->integer('id_user')->unsigned();
$table->foreign('id_user')->references('id')->on('users');
$table->integer('order_id')->unsigned();
$table->unique(array('id_user', 'order_id'));
$table->increments('id');
$table->string('status');
$table->string('payment_method')->nullable();
$table->double('change')->nullable();
$table->double('delivery_tax')->nullable();
$table->integer('id_client')->unsigned();
$table->foreign('id_client')->references('id')->on('client');
$table->double('total')->unsingned();
$table->string('complement')->nullable();
$table->string('zip_code')->nullable();
$table->string('housing')->nullable();
$table->string('number')->nullable();
$table->string('phone')->nullable();
$table->timestamps();
});
Schema::create('order_itens', function (Blueprint $table) {
$table->increments('id');
$table->integer('quantity');
$table->string('obs')->nullable();
$table->integer('id_drink')->unsigned()->nullable();
$table->foreign('id_drink')->references('id')->on('drink');
$table->integer('id_pizza')->unsigned()->nullable();
$table->foreign('id_pizza')->references('id')->on('pizza');
$table->integer('order_id');
$table->foreign('order_id')->references('id')->on('order');
$table->timestamps();
});
}
/**
* Reverse the migrations.
*
* @return void
*/
public function down()
{
Schema::dropIfExists('users');
}
}