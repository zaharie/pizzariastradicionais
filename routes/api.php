<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('/pedidos/novoPedido/usuario/{id?}',array('as' => 'pedidoInsert','uses'=>'OrderController@insert'));
Route::post('/pedidos/verificarPedido/usuario/{id?}',array('as' => 'pedidoVerify','uses'=>'OrderController@verifyOrder'));


Route::get('/pedidos/insert/',array('as' => 'teste','uses'=>'OrderController@insert'));










Route::get('/pizza/tamanhos/usuario/{id?}',array('as' => 'apiTamanhos','uses'=>'Pizza_sizeController@api'));
Route::get('/pizza/sabor/usuario/{id?}',array('as' => 'apiSabores','uses'=>'FlavorController@list'));
Route::get('/pizza/sabor/categorias/usuario/{id?}',array('as' => 'apiSaboresCategoria','uses'=>'CategoryController@list'));
Route::get('/bebidas/usuario/{id?}',array('as' => 'apiBebidas','uses'=>'DrinkController@list'));
Route::get('/pizza/bordas/{id?}',array('as' => 'apiBordas','uses'=>'BorderController@list'));






