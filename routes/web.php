<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/pedidos',array('as' => 'produtoCadastrar','uses'=>'UserController@formCadastrarProduto'));



Route::group(['middleware' => 'auth'], function () {

    Route::get('/pedidos',array('as' => 'pedidos','uses'=>'OrderController@orders'));
    Route::get('/pedidos/detalhesPendentes/{id?}',array('as' => 'pedidoDetalhesPendentes','uses'=>'OrderController@detalhesPendentes'));
    Route::get('/pedidos/pedidos/Pendentes/buscar/{id?}',array('as' => 'pedidoBuscarPendentes','uses'=>'OrderController@buscarPendentes'));

    Route::get('/pedidos/imprimir/{id?}',array('as' => 'imprimirPedido','uses'=>'OrderController@imprimir'));




    Route::get('/pedidos/pedidosAprovados/',array('as' => 'pedidosAprovados','uses'=>'OrderController@ordersAprovados'));
    Route::get('/pedidos/pedidoDetalhesAprovados/{id?}',array('as' => 'pedidoAprovadoDetalhes','uses'=>'OrderController@detalhesAprovados'));
    Route::get('/pedidos/pedidos/Aprovados/buscar/{id?}',array('as' => 'pedidoBuscarAprovados','uses'=>'OrderController@buscarAprovados'));



    Route::get('/pedidos/pedidosFinalizados/',array('as' => 'pedidosFinalizados','uses'=>'OrderController@ordersFinalizados'));
    Route::get('/pedidos/finalizar/{id?}',array('as' => 'pedidoFinalizar','uses'=>'OrderController@finalizar'));
    Route::get('/pedidos/finalizarTodos',array('as' => 'pedidosFinalizar','uses'=>'OrderController@finalizarTodos'));
    Route::get('/pedidos/pedidoDetalhesFinalizados/{id?}',array('as' => 'pedidoDetalhesFinalizados','uses'=>'OrderController@detalhesFinalizados'));
    Route::get('/pedidos/pedidos/Finalizadosbuscar/{id?}',array('as' => 'pedidoBuscarFinalizados','uses'=>'OrderController@buscarFinalizados'));








    Route::get('/pedidos/aprovar/{id?}',array('as' => 'pedidoAprovar','uses'=>'OrderController@aprovar'));
    Route::get('/pedidos/recusar/{id?}/{comentario?}',array('as' => 'pedidoRecusar','uses'=>'OrderController@recusar'));


    Route::get('/pizza/tamanho',array('as' => 'pizzaTamahos','uses'=>'Pizza_sizeController@Sizes'));
    Route::get('/pizza/tamanho/buscar/{nome?}',array('as' => 'tamanhoBuscar','uses'=>'Pizza_sizeController@search'));
    Route::get('/pizza/tamanho/alterar/{id?}',array('as' => 'tamanhoAlterar','uses'=>'Pizza_sizeController@formUpdate'));
    Route::get('/pizza/tamanho/cadastrar',array('as' => 'pizzaTamanho','uses'=>'Pizza_sizeController@formInsert'));
    Route::post('/pizza/tamanho/alterar/',array('as' => 'PizzaTamanhoAlterar','uses'=>'Pizza_sizeController@update'));
    Route::post('/pizza/tamanho/cadastrar',array('as' => 'pizzaTamanhoInsert','uses'=>'Pizza_sizeController@insert'));
    Route::get('/pizza/tamanho/deletar/{id?}',array('as' => 'pizzaTamanhoDelete','uses'=>'Pizza_sizeController@delete'));


    Route::get('/pizza/sabor',array('as' => 'sabores','uses'=>'FlavorController@flavors'));
    Route::get('/pizza/sabor/buscar/{nome?}',array('as' => 'saborBuscar','uses'=>'FlavorController@search'));
    Route::get('/pizza/sabor/alterar/{id?}',array('as' => 'saborAlterar','uses'=>'FlavorController@formUpdate'));
    Route::get('/pizza/sabor/cadastrar',array('as' => 'sabor','uses'=>'FlavorController@formInsert'));
    Route::post('/pizza/sabor/alterar/',array('as' => 'saborPostAlterar','uses'=>'FlavorController@update'));
    Route::post('/pizza/sabor/cadastrar',array('as' => 'saborInsert','uses'=>'FlavorController@insert'));
    Route::get('/pizza/sabor/deletar/{id?}',array('as' => 'saborDelete','uses'=>'FlavorController@delete'));


    Route::get('/pizza/borda',array('as' => 'bordas','uses'=>'BorderController@borders'));
    Route::get('/pizza/borda/buscar/{nome?}',array('as' => 'bordaBuscar','uses'=>'BorderController@search'));
    Route::get('/pizza/borda/alterar/{id?}',array('as' => 'bordaAlterar','uses'=>'BorderController@formUpdate'));
    Route::get('/pizza/borda/cadastrar',array('as' => 'borda','uses'=>'BorderController@formInsert'));
    Route::post('/pizza/borda/alterar/',array('as' => 'bordaPostAlterar','uses'=>'BorderController@update'));
    Route::post('/pizza/borda/cadastrar',array('as' => 'bordaInsert','uses'=>'BorderController@insert'));
    Route::get('/pizza/borda/deletar/{id?}',array('as' => 'bordaDelete','uses'=>'BorderController@delete'));

    Route::get('/moradia',array('as' => 'moradias','uses'=>'HousingController@houses'));
    Route::get('/moradia/buscar/{nome?}',array('as' => 'moradiaBuscar','uses'=>'HousingController@search'));
    Route::get('/moradia/alterar/{id?}',array('as' => 'moradiaAlterar','uses'=>'HousingController@formUpdate'));
    Route::get('/pizza/moradia/cadastrar',array('as' => 'moradia','uses'=>'HousingController@formInsert'));
    Route::post('/moradia/alterar/',array('as' => 'moradiaPostAlterar','uses'=>'HousingController@update'));
    Route::post('/moradia/cadastrar',array('as' => 'moradiaInsert','uses'=>'HousingController@insert'));
    Route::get('/moradia/deletar/{id?}',array('as' => 'moradiaDelete','uses'=>'HousingController@delete'));


    Route::get('/delivery',array('as' => 'delivery','uses'=>'DeliveryController@formInsert'));
    Route::post('/delivery/update',array('as' => 'deliveryUpdate','uses'=>'DeliveryController@Update'));
    Route::post('/delivery/cadastrar',array('as' => 'deliveryInsert','uses'=>'DeliveryController@insert'));

    Route::get('/pagamentos',array('as' => 'pagamentos','uses'=>'PaymentController@payments'));
    Route::get('/pagamentos/buscar/{nome?}',array('as' => 'pagamentoBuscar','uses'=>'PaymentController@search'));
    Route::get('/pagamentos/alterar/{id?}',array('as' => 'pagamentoAlterar','uses'=>'PaymentController@formUpdate'));
    Route::get('/pagamentos/cadastrar',array('as' => 'pagamento','uses'=>'PaymentController@formInsert'));
    Route::post('/pagamentos/alterar/',array('as' => 'pagamentoPostAlterar','uses'=>'PaymentController@update'));
    Route::post('/pagamentos/cadastrar',array('as' => 'pagamentoInsert','uses'=>'PaymentController@insert'));
    Route::get('/pagamentos/deletar/{id?}',array('as' => 'pagamentoDelete','uses'=>'PaymentController@delete'));


    Route::get('/bebidas',array('as' => 'bebidas','uses'=>'DrinkController@drinks'));
    Route::get('/bebidas/buscar/{nome?}',array('as' => 'bebidaBuscar','uses'=>'DrinkController@search'));
    Route::get('/bebidas/alterar/{id?}',array('as' => 'bebidaAlterar','uses'=>'DrinkController@formUpdate'));
    Route::get('/bebidas/cadastrar',array('as' => 'bebida','uses'=>'DrinkController@formInsert'));
    Route::post('/bebidas/alterar/',array('as' => 'bebidaPostAlterar','uses'=>'DrinkController@update'));
    Route::post('/bebidas/cadastrar',array('as' => 'bebidaInsert','uses'=>'DrinkController@insert'));
    Route::get('/bebidas/deletar/{id?}',array('as' => 'bebidaDelete','uses'=>'DrinkController@delete'));

    Route::get('/sabor/categoria',array('as' => 'categorias','uses'=>'CategoryController@categories'));
    Route::get('/sabor/categoria/buscar/{nome?}',array('as' => 'categoriaBuscar','uses'=>'CategoryController@search'));
    Route::get('/sabor/categoria/alterar/{id?}',array('as' => 'categoriaAlterar','uses'=>'CategoryController@formUpdate'));
    Route::get('/sabor/categoria/cadastrar',array('as' => 'categoria','uses'=>'CategoryController@formInsert'));
    Route::post('/sabor/categoria/alterar/',array('as' => 'categoriaPostAlterar','uses'=>'CategoryController@update'));
    Route::post('/sabor/categoria/cadastrar',array('as' => 'categoriaInsert','uses'=>'CategoryController@insert'));
    Route::get('/sabor/categoria/deletar/{id?}',array('as' => 'categoriaDelete','uses'=>'CategoryController@delete'));




    Route::get('/Dash',array('as' => 'dash','uses'=>'UserController@Dash'));
        Route::post('/Dash/alterar',array('as' => 'alterar','uses'=>'UserController@alterar'));

    Route::get('/dados/consumo',array('as' => 'consumo','uses'=>'UserController@consumoHoje'));
    Route::get('/dados/consumo/mes',array('as' => 'consumoMes','uses'=>'UserController@consumoMes'));
    Route::get('/dados/consumo/ano',array('as' => 'consumoAno','uses'=>'UserController@consumoAno'));
    Route::get('/dados/consumo/total',array('as' => 'consumoTotal','uses'=>'UserController@consumoTotal'));

});






