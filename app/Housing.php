<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Housing extends Model
{
    public $table = "housing";


    public function validaCadastro()
    {
        return array('nome' => 'required|min:3','taxa' => 'required|numeric');
    }

}
