<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = "category";


    public function validaCadastro()
    {
        return array('nome' => 'required|min:3');
    }

}
