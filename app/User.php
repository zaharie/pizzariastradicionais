<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','server_url','cep','cnpj','phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function validaAlterar()
    {
        return array('name' => 'required|min:3','phone' => 'required','waiting_time' => 'required|numeric','cnpj' => 'required|numeric','cep' => 'required|numeric');
    }

}
