<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
   public $table = "delivery_cost";
   public $timestamps = false;

}
