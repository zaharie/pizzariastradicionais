<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Flavor extends Model
{
    public $table = "flavor";


    public function validaCadastro()
    {
        return array('nome' => 'required|min:3','preco' => 'required|numeric','preco_promocional' => 'required|numeric',"img" => 'required','categoria' => 'required','type' => 'required');
    }
    public function validaAlterar()
    {
        return array('nome' => 'required|min:3','preco' => 'required|numeric','preco_promocional' => 'required|numeric','categoria' => 'required','type' => 'required');
    }
    public function image()
    {
        return $this->hasMany('App\Image','id_flavor','id');
    }
    public function categories()
    {
        return $this->belongsToMany(Category::class,'flavor_category','id_flavor',"id_category");
    }


}
