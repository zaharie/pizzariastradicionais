<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Border extends Model
{
   public $table = "border";

    public function validaCadastro()
    {
        return array('nome' => 'required|min:3');
    }
}
