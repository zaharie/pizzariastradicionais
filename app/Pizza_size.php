<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pizza_size extends Model
{

    public $table = "pizza_size";

    public function validaCadastro()
    {
        return array('nome' => 'required|min:3','preco' => 'required|numeric','preco_promocional' => 'required|numeric','price_border' => 'required|numeric','quantidade_pedacos' => 'required|numeric','quantidade_sabores' => 'required|numeric');
    }
}
