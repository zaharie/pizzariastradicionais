<?php

namespace App\Http\Controllers;
use App\Flavor;
use App\Flavor_category;
use Validator;
use App\Category;
use Illuminate\Http\Request;
use Redirect;
use View;
use Auth;

class CategoryController extends Controller
{
    private $flavor;
    private $category;
    private $flavor_category;
    function __construct(){
        $this->category = new category();
        $this->flavor = new Flavor();
        $this->flavor_category = new Flavor_category();
    }



    public function list(Request $req){
        return response()->json($this->category->where('id_user','=',$req->id)
            ->where('active','=',true)->orderBy('name', 'ASC')->get());

    }

    public function categories(){
        return View::make('Category/categories')->with('categories',$this->category->where("id_user",'=',Auth::id())->where('active','=','true')->paginate(10));
    }
    public function formInsert(){
        return View::make('Category/formInsert');
    }
    public function api(Request $req){

        return response()->json($this->category->where('id_user','=',$req->id)->get());
    }
    public function insert(Request $req){
        $validator = Validator::make($req->all(), $this->category->validaCadastro());
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $this->category->id_user = Auth::id();
        $this->category->name = $req->nome;
        $this->category->active = true;
        $this->category->save();
        return Redirect::back()->with('mensagem', 'Cadastrado com sucesso !');
    }
    public function update(Request $req){
        $req->preco = str_replace(",",".",$req->preco);
        $req->preco_promocional = str_replace(",",".",$req->preco_promocional);
        $validator = Validator::make($req->all(), $this->category->validaCadastro());
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }
        $category = $this->category->find($req->id);
        $category->id_user = Auth::id();
        $category->name = $req->nome;
        $category->save();
        return Redirect::back()->with('mensagem', 'Alterado com sucesso !');
    }
    public function formUpdate(Request $req){
        return View::make("Category/formUpdate")->with('category', $this->category->where('id_user', '=', Auth::id())->where('id',$req->id)->first());
    }
    public function search(Request $req){
        if(!$req->nome){
            return View::make('Category/categories')->with('categories',$this->category->where("id_user",'=',Auth::id())->where("active",'=',true)->paginate(10));
        }
        return View::make('Category/categories')->with('categories',$this->category->where("id_user",'=',Auth::id())->where('name', 'ilike', "%$req->nome%")->where("active",'=',true)->paginate(10));
    }

    public function delete(Request $req){
        $category = $this->category->find($req->id);
        $this->flavor_category->where('id_category','=',$category->id)->delete();
        $category->delete();
        return Redirect::back()->with('mensagem', 'Removido com sucesso !');
    }
}
