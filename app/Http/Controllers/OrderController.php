<?php

namespace App\Http\Controllers;

use App\Border;
use App\Client;
use App\Delivery;
use App\Drink;
use App\Flavor;
use App\User;
use GuzzleHttp\Client as clientHtpp;
use App\Flavor_pizza;
use App\OrderItens;
use App\Pizza;
use App\Pizza_size;
use Illuminate\Http\Request;
use App\Order;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use PhpParser\Node\Expr\New_;
use View;
use App\Housing;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public $order;
    public $orderItens;
    public $client;
    public $drink;
    public $flavor_pizza;
    public $border;
    public $flavor;
    public $user;
    public $housing;
    public $pizza;
    function __construct(){
        $this->drink = new Drink();
        $this->flavor = new Flavor();
        $this->order = new Order();
        $this->orderItens = new OrderItens();
        $this->client = new Client();
        $this->border = new Border();
        $this->pizza = new Pizza();
        $this->user = new User();
        $this->size = new Pizza_size();
        $this->housing = new Housing();
        $this->flavor_pizza = new Flavor_pizza();
    }

    public function buscarAprovados(Request $req){
        if(!$req->id){
            return View::make('Order/aprovados/orders')->with('orders',$this->order->where('id_user',Auth::id())->where('status','=',"Aprovado")->paginate(10));
        }

        return View::make('Order/aprovados/orders')->with('orders',$this->order->where('id_user',Auth::id())->where('order_id','=',$req->id)
            ->where("status",'=',"Aprovado")->paginate(10));
    }
    public function buscarFinalizados(Request $req){
        if(!$req->id or is_numeric($req->id)){
            return View::make('Order/finalizados/orders')->with('orders',$this->order->where('id_user',Auth::id())->where('status','=',"Finalizado")->paginate(10));
        }

        return View::make('Order/finalizados/orders')->with('orders',$this->order->where('id_user',Auth::id())->where('order_id','=',$req->id) ->where("status",'=',"Finalizado")->paginate(10));
    }
    public function buscarPendentes(Request $req){
        if(!$req->id){
            return View::make('Order/pendentes/orders')->with('orders',$this->order->where('id_user',Auth::id())->where('status','=',"Pendente")->paginate(10));
        }

        return View::make('Order/pendentes/orders')->with('orders',$this->order->where('id_user',Auth::id())->where('order_id','=',$req->id) ->where("status",'=',"Pendente")->paginate(10));
    }

    public function orders(){

        return View::make('Order/pendentes/orders')->with('orders',$this->order->where('id_user',Auth::id())->where('status','=',"Pendente")->paginate(10));
    }


    public function ordersAprovados(){
        return View::make('Order/aprovados/orders')->with('orders',$this->order->where('id_user',Auth::id())->where('status','=',"Aprovado")->paginate(10));
    }
    public function ordersFinalizados(){
        return View::make('Order/finalizados/orders')->with('orders',$this->order->where('id_user',Auth::id())->where('status','=',"Finalizado")->paginate(10));
    }

    public function detalhesAprovados(Request $req){

        $pedido = $this->order->where('id','=',$req->id)->first();
        $taxaCasa = $this->housing->where('name','=',$pedido->housing)->first();
        $client = $this->client->where('id','=',$pedido->id_client)->first();
        $endereco = json_decode(file_get_contents("http://viacep.com.br/ws/$pedido->zip_code/json/"));
        $pizzas = $this->orderItens->where("order_id",'=',$req->id)->whereNotNull("order_itens.id_pizza")
            ->join("pizza",'order_itens.id_pizza','=','pizza.id')
            ->get();
        $p = array();
        foreach ($pizzas as $pizza){
            if($this->pizza->join("border",'pizza.border','=','border.id')->where("pizza.id",'=',$pizza->id_pizza)->first() !== null) {

                $flavors = $this->flavor->join("flavor_pizza", 'flavor.id', '=', 'flavor_pizza.id_flavor')
                    ->join("pizza", 'flavor_pizza.id_pizza', '=', 'pizza.id')->join("border","pizza.border",'=','border.id')
                    ->join("pizza_size", 'pizza.id_size', '=', 'pizza_size.id')->where("pizza.id", '=', $pizza->id)
                    ->get(["pizza_size.name as size","flavor.name as flavor",'border.name as borda', "flavor.price as flavor_price", "pizza_size.price as size_price","pizza_size.price_border as bordaPreco"]);

            }else{
                $flavors = $this->flavor->join("flavor_pizza", 'flavor.id', '=', 'flavor_pizza.id_flavor')
                    ->join("pizza", 'flavor_pizza.id_pizza', '=', 'pizza.id')
                    ->join("pizza_size", 'pizza.id_size', '=', 'pizza_size.id')->where("pizza.id", '=', $pizza->id)
                    ->get(["pizza_size.name as size", "flavor.name as flavor", "flavor.price as flavor_price", "pizza_size.price as size_price"]);
            }
            $sabores = array();
            $saboresPreco = array();
            foreach ($flavors as $flavor){
                array_push($sabores,$flavor->flavor);
                array_push($saboresPreco,$flavor->flavor_price);
                $pizza->borda = $flavor->borda;
                $pizza->size = $flavor->size;
                $pizza->border_price = $flavor->bordaPreco;
                $pizza->size_price = $flavor->size_price;
            }

            $pizza->sabores_price = $saboresPreco;
            $pizza->sabores = $sabores;
            array_push($p,$pizza);
        }
        $drinks = $this->order->join("order_itens",'order.id','=','order_itens.order_id')
            ->join('drink','order_itens.id_drink','=','drink.id')
            ->where("order_itens.order_id",'=',$req->id)
            ->get();
        return View::make('Order/aprovados/detalhes')->with('pizzas',$p)->with('taxaMoradia',$taxaCasa)->with("pedido",$pedido)->with('drinks',$drinks)->with('endereco',$endereco)->with('client',$client);
    }


    public function detalhesFinalizados(Request $req){
        $pedido = $this->order->where('id','=',$req->id)->first();
        $taxaCasa = $this->housing->where('name','=',$pedido->housing)->first();
        $client = $this->client->where('id','=',$pedido->id_client)->first();
        $endereco = json_decode(file_get_contents("http://viacep.com.br/ws/$pedido->zip_code/json/"));
        $pizzas = $this->orderItens->where("order_id",'=',$req->id)->whereNotNull("order_itens.id_pizza")
            ->join("pizza",'order_itens.id_pizza','=','pizza.id')
            ->get();
        $p = array();
        foreach ($pizzas as $pizza){
            if($this->pizza->join("border",'pizza.border','=','border.id')->where("pizza.id",'=',$pizza->id_pizza)->first() !== null) {

                $flavors = $this->flavor->join("flavor_pizza", 'flavor.id', '=', 'flavor_pizza.id_flavor')
                    ->join("pizza", 'flavor_pizza.id_pizza', '=', 'pizza.id')->join("border","pizza.border",'=','border.id')
                    ->join("pizza_size", 'pizza.id_size', '=', 'pizza_size.id')->where("pizza.id", '=', $pizza->id)
                    ->get(["pizza_size.name as size","flavor.name as flavor",'border.name as borda', "flavor.price as flavor_price", "pizza_size.price as size_price","pizza_size.price_border as bordaPreco"]);

            }else{
                $flavors = $this->flavor->join("flavor_pizza", 'flavor.id', '=', 'flavor_pizza.id_flavor')
                    ->join("pizza", 'flavor_pizza.id_pizza', '=', 'pizza.id')
                    ->join("pizza_size", 'pizza.id_size', '=', 'pizza_size.id')->where("pizza.id", '=', $pizza->id)
                    ->get(["pizza_size.name as size", "flavor.name as flavor", "flavor.price as flavor_price", "pizza_size.price as size_price"]);
            }
            $sabores = array();
            $saboresPreco = array();
            foreach ($flavors as $flavor){
                array_push($sabores,$flavor->flavor);
                array_push($saboresPreco,$flavor->flavor_price);
                $pizza->borda = $flavor->borda;
                $pizza->size = $flavor->size;
                $pizza->border_price = $flavor->bordaPreco;
                $pizza->size_price = $flavor->size_price;
            }

            $pizza->sabores_price = $saboresPreco;
            $pizza->sabores = $sabores;
            array_push($p,$pizza);
        }
        $drinks = $this->order->join("order_itens",'order.id','=','order_itens.order_id')
            ->join('drink','order_itens.id_drink','=','drink.id')
            ->where("order_itens.order_id",'=',$req->id)
            ->get();
        return View::make('Order/finalizados/detalhes')->with('pizzas',$p)->with('taxaMoradia',$taxaCasa)->with("pedido",$pedido)->with('drinks',$drinks)->with('endereco',$endereco)->with('client',$client);
    }


    public function finalizar(Request $req){
      $pedido =  $this->order->where("id",'=',$req->id)->first();
      $pedido->status = "Finalizado";
      $pedido->save();
        return Redirect::route('pedidosAprovados');
    }


    public function finalizarTodos(Request $req){
        $pedido =  $this->order->where("status",'=',"Aprovado")->where("id_user",'=',Auth::id())->update(['status' => 'Finalizado']);
        return Redirect::route('pedidosAprovados');
    }


    public function detalhesPendentes(Request $req){
        $pedido = $this->order->where('id','=',$req->id)->first();
        $taxaCasa = $this->housing->where('name','=',$pedido->housing)->first();
        $client = $this->client->where('id','=',$pedido->id_client)->first();
        $endereco = json_decode(file_get_contents("http://viacep.com.br/ws/$pedido->zip_code/json/"));
        $pizzas = $this->orderItens->where("order_id",'=',$req->id)->whereNotNull("order_itens.id_pizza")
            ->join("pizza",'order_itens.id_pizza','=','pizza.id')
            ->get();
        $p = array();
        foreach ($pizzas as $pizza){
            if($this->pizza->join("border",'pizza.border','=','border.id')->where("pizza.id",'=',$pizza->id_pizza)->first() !== null) {

                $flavors = $this->flavor->join("flavor_pizza", 'flavor.id', '=', 'flavor_pizza.id_flavor')
                    ->join("pizza", 'flavor_pizza.id_pizza', '=', 'pizza.id')->join("border","pizza.border",'=','border.id')
                    ->join("pizza_size", 'pizza.id_size', '=', 'pizza_size.id')->where("pizza.id", '=', $pizza->id)
                    ->get(["pizza_size.name as size","flavor.name as flavor",'border.name as borda', "flavor.price as flavor_price", "pizza_size.price as size_price","pizza_size.price_border as bordaPreco"]);

            }else{
                $flavors = $this->flavor->join("flavor_pizza", 'flavor.id', '=', 'flavor_pizza.id_flavor')
                    ->join("pizza", 'flavor_pizza.id_pizza', '=', 'pizza.id')
                    ->join("pizza_size", 'pizza.id_size', '=', 'pizza_size.id')->where("pizza.id", '=', $pizza->id)
                    ->get(["pizza_size.name as size", "flavor.name as flavor", "flavor.price as flavor_price", "pizza_size.price as size_price"]);
        }
            $sabores = array();
            $saboresPreco = array();
            foreach ($flavors as $flavor){
                array_push($sabores,$flavor->flavor);
                array_push($saboresPreco,$flavor->flavor_price);
                $pizza->borda = $flavor->borda;
                $pizza->size = $flavor->size;
                $pizza->border_price = $flavor->bordaPreco;
                $pizza->size_price = $flavor->size_price;
            }

            $pizza->sabores_price = $saboresPreco;
            $pizza->sabores = $sabores;
            array_push($p,$pizza);
        }
        $drinks = $this->order->join("order_itens",'order.id','=','order_itens.order_id')
            ->join('drink','order_itens.id_drink','=','drink.id')
            ->where("order_itens.order_id",'=',$req->id)
            ->get();
        return View::make('Order/pendentes/detalhes')->with('pizzas',$p)->with('taxaMoradia',$taxaCasa)->with("pedido",$pedido)->with('drinks',$drinks)->with('endereco',$endereco)->with('client',$client);

    }



    public function aprovar(Request $req){
      $user = $this->user->where('id','=',Auth::id())->first();
        $order = $this->order::find($req->id);
        $order->status = "Aprovado";
        $order->save();
        $orders =  $this->order->where('order.id_user','=',Auth::id())->where("order.id",'=',$req->id)
            ->join("client",'order.id_client','=','client.id')
            ->select(["order.id as order_id","client.name as name","order.zip_code as zip_code","order.phone as phone",'order.total as total',"order.complement as complement","order.housing as housing"
                ,"order.number as number","client.profile_pic as profile_pic",'order.delivery_tax as delivery_tax','order.payment_method as payment_method',"client.id_face as sender"])->first();




        $housing = $this->housing->where("name",'=',$orders->housing)->first();
        $pizzas = $this->orderItens->where("order_id",'=',$req->id)->whereNotNull("order_itens.id_pizza")
            ->join("pizza",'order_itens.id_pizza','=','pizza.id')
            ->get();
        $p = array();
        foreach ($pizzas as $pizza){
            if($this->pizza->join("border",'pizza.border','=','border.id')->where("pizza.id",'=',$pizza->id_pizza)->first() !== null) {
                $flavors = $this->flavor->join("flavor_pizza", 'flavor.id', '=', 'flavor_pizza.id_flavor')
                    ->join("pizza", 'flavor_pizza.id_pizza', '=', 'pizza.id')->join("border","pizza.border",'=','border.id')
                    ->join("pizza_size", 'pizza.id_size', '=', 'pizza_size.id')->where("pizza.id", '=', $pizza->id)
                    ->get(["pizza_size.name as size","flavor.name as flavor",'border.name as borda', "flavor.price as flavor_price", "pizza_size.price as size_price","pizza_size.price_border as bordaPreco
                    ","pizza_size.quantity_flavors as qtd_size"]);

            }else{
                $flavors = $this->flavor->join("flavor_pizza", 'flavor.id', '=', 'flavor_pizza.id_flavor')
                    ->join("pizza", 'flavor_pizza.id_pizza', '=', 'pizza.id')
                    ->join("pizza_size", 'pizza.id_size', '=', 'pizza_size.id')->where("pizza.id", '=', $pizza->id)
                    ->get(["pizza_size.name as size", "flavor.name as flavor", "flavor.price as flavor_price", "pizza_size.price as size_price","pizza_size.quantity_flavors as qtd_size"]);
            }
            $sabores = array();
            $preco = 0;
            foreach ($flavors as $flavor){
                $preco += $flavor->flavor_price + $flavor->size_price;
                $pizza->borda = $flavor->borda;
                $pizza->size = $flavor->size;
                $pizza->border_price = $flavor->bordaPreco;
                $pizza->sabor_qtd = $flavor->qtd_size;

            }
            $pizza->preco = $preco/$pizza->sabor_qtd;
            $pizza->sabores = $sabores;
            array_push($p,$pizza);
        }
        $drinks = $this->order->join("order_itens",'order.id','=','order_itens.order_id')
            ->join('drink','order_itens.id_drink','=','drink.id')
            ->where("order_itens.order_id",'=',$req->id)
            ->get();
        $client = new clientHtpp;



        $r = $client->post("https://$user->server_url/enfoca/webhook/pedido/aprovado",
            ['json' => [
                "order" => $orders,
                "pizzas" => $p,
                "drinks" => $drinks,
                "housing" => $housing,
                "pizzaria" => $user
            ]]);;
        return Redirect::route('pedidos');

    }
    public function recusar(Request $req){
        $user = $this->user::find(Auth::id());
if(!$req->comentario){



    $req->comentario = "Ocorreu um problema";
    }
        $order = $this->order->where("id_user","=",Auth::id())->where("id",'=',$req->id)->first();
        $order->status = "Recusado";
        $order->save();
        $orders =  $this->order->where('order.id_user','=',Auth::id())->where("order.id",'=',$req->id)
            ->join("client",'order.id_client','=','client.id')
            ->select(["order.id as order_id","client.name as name","order.zip_code as zip_code","order.phone as phone",'order.total as total',"order.complement as complement","order.housing as housing"
                ,"order.number as number",'order.payment_method as payment_method',"client.id_face as sender"])->first();

        $client = new clientHtpp;
        $r = $client->post("https://$user->server_url/enfoca/webhook/pedido/recusado/",
            ['json' => ["order" => $orders,"motivo" => $req->comentario]]);;
        return Redirect::route('pedidos');
    }


    public function verifyOrder(Request $req){
        $pedido = $req->input("pedido");
        $result = $this->calculaTotal($pedido,$req->id);
        return response()->json(["total" => $result["total"]]);
    }


    protected function calculaTotal($pedido,$id){
        $total = 0;
        $user = new User();
        /*$cep = explode($pedido[cep]);
        $newCep = $cep[1]+$cep[2]+$cep[3]+$cep[4]+$cep[5]+"-"+$cep[6]+$cep[7]+$cep[8];
        */
        $user = $user->where("id",'=',$id)->first();
        $json_url = "http://maps.googleapis.com/maps/api/distancematrix/json?origins=$user->cep&destinations=$pedido[cep]&mode=driving&language=en_us&sensor=false";
        $endereco = json_decode(file_get_contents($json_url));
        if($endereco->rows[0]->elements[0]->status != "NOT_FOUND") {
            $km = round($endereco->rows[0]->elements[0]->distance->value / 1000);
        }else{
            $km = 6;
        }
        $delivery = new Delivery();
        $delivery = $delivery->find($id);
        $taxaEntrega = round($km * $delivery->tax * 2);
        $total = $delivery->tax_min;
        if($km >= 5){
            $total = $taxaEntrega;
        }
        $taxaResidencia =  $this->housing->where("name",'=', $pedido["residencia"])->first();
        $total += $taxaResidencia["tax"];

        foreach($pedido["carrinho"] as $key => $item){
            if(array_key_exists("bebida",$item)){
                $drink = $this->drink->where("id",'=',$item["bebida"]['id'])->first();
                $total += $drink["price"] * $item['bebida']['qtd'];
            }
            if(array_key_exists("pizza",$item)){
                $size = $this->size->where("id","=",$item['pizza']['tamanhoId'])->first();
                if($item['pizza']['bordaId'] != 0) {
                    $total += $size->price_border * $item['pizza']["qtd"];
                }
                $saborValores = array();
                foreach ($item['pizza']['saboresId'] as $sabor){
                    $flavor = $this->flavor->where("id",'=',$sabor)->first();
                    array_push($saborValores,$flavor["price"]);
                }

                $precoFinalSabores  = 0;
                foreach ($saborValores as $preco){
                    $precoFinalSabores += $preco + $size["price"];
                }
                $total += $precoFinalSabores/count($item['pizza']['saboresId']) * $item['pizza']['qtd'];


            }

        }
        return array("total" => $total,"taxaResidencia" => $taxaResidencia,"delivery" => $taxaEntrega);

    }


    public function insert(Request $req){



        \DB::beginTransaction();
        $pedido = $req->input("pedido");
        $result = $this->calculaTotal($pedido,$req->id);

        if ($this->client->where('id_face', '=', $pedido["senderId"])->first() === null) {
            $this->client->name = $pedido['first_name'];
            $this->client->last_name = $pedido['last_name'];
            $this->client->profile_pic = $pedido['profile_pic'];
            $this->client->id_face = $pedido["senderId"];
            $this->client->status = true;
            $this->client->id_user =  $req->id;
            $this->client->save();
            $clientId = $this->client->id;
        }else{
            $client = $this->client->where('id_face', '=', $pedido["senderId"])->first();
            $clientId = $client->id;
        }



        $order = new Order();
        $nPedidos = $order->where("id_user",'=',$req->id)->count();
        $this->order->id_user = $req->id;
        $this->order->status = "Pendente";
        $this->order->id_client = $clientId;




        $this->order->order_id = $nPedidos + 1;
            $this->order->delivery_tax = $result["delivery"];
        $this->order->total = $result["total"];
        $this->order->complement = $pedido["complemento"];
        $this->order->zip_code = $pedido['cep'];
        $this->order->housing = $pedido['residencia'];
        $this->order->payment_method = $pedido["metodoPagamento"];
        $this->order->number = $pedido["numeroCasa"];
        $this->order->phone = $pedido["telefone"];
        if(array_key_exists("troco",$pedido) && is_numeric($pedido["troco"])){
            $this->order->change = $pedido["troco"];
        }
        $this->order->save();
        foreach($pedido["carrinho"] as $key => $item){
            if(array_key_exists("bebida",$item)){
                $orderItens = New OrderItens();
                $orderItens->id_drink = $item["bebida"]["id"];
                $orderItens->order_id = $this->order->id;
                $orderItens->quantity = $item["bebida"]["qtd"];
                $orderItens->save();
            }
            if(array_key_exists("pizza",$item)){
                $pizza = New Pizza();
                $orderItens = New OrderItens();
                $pizza->id_user = $req->id;
                if($item["pizza"]["obs"]) {
                    $pizza->obs = $item["pizza"]["obs"];
                }
                $pizza->id_size =  $item["pizza"]["tamanhoId"];
                if($item["pizza"]["bordaId"] != 0) {
                    $pizza->border = $item["pizza"]["bordaId"];
                }
                $pizza->save();
                $orderItens->id_pizza = $pizza->id;
                $orderItens->order_id = $this->order->id;
                $orderItens->quantity = $item["pizza"]["qtd"];
                $orderItens->save();
                foreach ($item["pizza"]["saboresId"] as $sabor){
                    $flavor_pizza = New Flavor_pizza();
                    $flavor_pizza->id_pizza = $pizza->id;
                    $flavor_pizza->id_flavor = $sabor;
                    $flavor_pizza->save();
                }
            }
        }
        DB::commit();
         return response(200);
    }
      public function imprimir(Request $req){



          $pedido = $this->order->where("id",'=',$req->id)->where("id_user",'=',Auth::id())->first();
          $taxaCasa = $this->housing->where('name','=',$pedido->housing)->first();
          $client = $this->client->where('id','=',$pedido->id_client)->first();
          $endereco = json_decode(file_get_contents("http://viacep.com.br/ws/$pedido->zip_code/json/"));
          $pizzas = $this->orderItens->where("order_id",'=',$req->id)->whereNotNull("order_itens.id_pizza")
              ->join("pizza",'order_itens.id_pizza','=','pizza.id')
              ->get();
          $p = array();
          foreach ($pizzas as $pizza){
              if($this->pizza->join("border",'pizza.border','=','border.id')->where("pizza.id",'=',$pizza->id_pizza)->first() !== null) {

                  $flavors = $this->flavor->join("flavor_pizza", 'flavor.id', '=', 'flavor_pizza.id_flavor')
                      ->join("pizza", 'flavor_pizza.id_pizza', '=', 'pizza.id')->join("border","pizza.border",'=','border.id')
                      ->join("pizza_size", 'pizza.id_size', '=', 'pizza_size.id')->where("pizza.id", '=', $pizza->id)
                      ->get(["pizza_size.name as size","flavor.name as flavor",'border.name as borda', "flavor.price as flavor_price", "pizza_size.price as size_price","pizza_size.price_border as bordaPreco"]);

              }else{
                  $flavors = $this->flavor->join("flavor_pizza", 'flavor.id', '=', 'flavor_pizza.id_flavor')
                      ->join("pizza", 'flavor_pizza.id_pizza', '=', 'pizza.id')
                      ->join("pizza_size", 'pizza.id_size', '=', 'pizza_size.id')->where("pizza.id", '=', $pizza->id)
                      ->get(["pizza_size.name as size", "flavor.name as flavor", "flavor.price as flavor_price", "pizza_size.price as size_price"]);
              }
              $sabores = array();
              $saboresPreco = array();
              foreach ($flavors as $flavor){
                  array_push($sabores,$flavor->flavor);
                  array_push($saboresPreco,$flavor->flavor_price);
                  $pizza->borda = $flavor->borda;
                  $pizza->size = $flavor->size;
                  $pizza->border_price = $flavor->bordaPreco;
                  $pizza->size_price = $flavor->size_price;
              }

              $pizza->sabores_price = $saboresPreco;
              $pizza->sabores = $sabores;
              array_push($p,$pizza);
          }
          $drinks = $this->order->join("order_itens",'order.id','=','order_itens.order_id')
              ->join('drink','order_itens.id_drink','=','drink.id')
              ->where("order_itens.order_id",'=',$req->id)
              ->get();

          $pdf = \PDF::loadView('pdf.pedido',array('pizzas'=> $p,'taxaMoradia' => $taxaCasa,'pedido'=> $pedido,'drinks' => $drinks,'endereco' => $endereco,'client' => $client));

          ///$pdf = \App::make('dompdf.wrapper');
          //$pdf->loadHTML('<h1>Emporio</h1><h1>Item: Pizza.</h1><h1>Preço: 29,90.</h1><h1>Sabores: Calabresa/Chocolate/Rucula.</h1><h1>Quantidade: 1.</h1><hr><h1>Total: 29,90.</h1>');
          return $pdf->stream("$pedido->id");
      }


}
