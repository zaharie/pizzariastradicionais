<?php

namespace App\Http\Controllers;

use App\Category;
use App\Image;
use Illuminate\Http\Request;
use App\Flavor;
use Illuminate\Support\Facades\Auth;
use View;
use Redirect;
use Validator;
use Illuminate\Support\Facades\Storage;
use App\Flavor_category;

class FlavorController extends Controller
{

    protected $flavor;
    protected $image;
    protected $flavor_category;
    protected $category;

function __construct(){
    $this->flavor = New Flavor();
    $this->image = New Image();
    $this->category = New Category();
    $this->flavor_category = New Flavor_category();
}

    public function api(Request $req){

        return response()->json($this->flavor->where('id_user','=',$req->id)->orderBy('name', 'desc')->get());
    }


    public function list(Request $req){
        return response()->json($this->flavor->where('flavor.id_user','=',$req->id)
            ->where('flavor.active','=',true)->with('image')->with('categories')->orderBy('flavor.name', 'ASC')->get());

    }

public function flavors(){
   return View::make('Flavor/flavors')->with('flavors',$this->flavor->where('id_user','=',Auth::id())->where("active",'=',true)->orderBy('name','asc')->paginate(10));
}
public function formInsert(Request $req){
return View::make('Flavor/formInsert')->with('categorias',$this->category->all())->with('categorias',$this->category->select('name','id')->get()->toJson());
}
public function insert(Request $req){
    $req->preco = str_replace(",",".",$req->preco);
    $req->preco_promocional = str_replace(",",".",$req->preco_promocional);
    $validator = Validator::make($req->all(), $this->flavor->validaCadastro());
    if ($validator->fails()) {

        return Redirect::back()->withErrors($validator)->withInput();
    }
    $this->flavor->name = $req->nome;
    $this->flavor->price = $req->preco;
    $this->flavor->id_user = Auth::id();
    $this->flavor->price_promotional = $req->preco_promocional;
    $this->flavor->active = true;
    $this->flavor->type = $req->type;
    $this->flavor->save();
    if (!empty($req->img)) {
        $imagens = $req->img;
        foreach ($imagens as $key => $value) {
            $img = \Intervention\Image\Facades\Image::make($value);
            $img->resize(60,60);
            $path = md5(microtime() . rand()) . (".") . ($value->extension());
            Storage::disk('public')->put($path, $img->encode('jpg',100));
            $imagem = new Image();
            $imagem->image = $path;
            $imagem->id_flavor = $this->flavor->id;
            $imagem->save();
        }
    }

    $categorias = explode(",",$req->categoria);
    foreach($categorias as $categoria){
        $flavor_category = new Flavor_category();
        $flavor_category->id_user = Auth::id();
        $flavor_category->id_flavor = $this->flavor->id;
        $flavor_category->id_category = $categoria;
        $flavor_category->save();

    }

    return Redirect::back()->with('mensagem', 'Cadastrado com sucesso !')->with('categorias',$this->category->all());
}



public function update(Request $req){
    $req->preco = str_replace(",",".",$req->preco);
    $req->preco_promocional = str_replace(",",".",$req->preco_promocional);
    $validator = Validator::make($req->all(), $this->flavor->validaAlterar());
    if ($validator->fails()) {
        return Redirect::back()->withErrors($validator)->withInput();
    }

    $this->flavor_category->where('id_flavor','=',$req->id)->delete();
    $this->flavor = $this->flavor->find($req->id);
    $this->flavor->name = $req->nome;
    $this->flavor->price = $req->preco;
    $this->flavor->id_user = Auth::id();
    $this->flavor->price_promotional = $req->preco_promocional;
    $this->flavor->promotion = $req->promocao;
    $this->flavor->active = true;
    $this->flavor->type = $req->type;
    $this->flavor->save();

    if (!empty($req->img)) {
        $this->image->where('id_flavor','=',$this->flavor->id)->delete();
        $imagens = $req->img;
        foreach ($imagens as $value) {

            $img = \Intervention\Image\Facades\Image::make($value);
            $img->resize(60,60);
            $path = md5(microtime() . rand()) . (".") . ($value->extension());
            Storage::disk('public')->put($path, $img->encode('jpg',100));
            $imagem = new Image();
            $imagem->image = $path;
            $imagem->id_flavor = $this->flavor->id;
            $imagem->save();

        }
    }

    $categorias = explode(",",$req->categoria);


    $this->flavor_category->where('id_flavor','=',$this->flavor->id)->delete();
    foreach($categorias as $categoria){
        $flavor_category = new Flavor_category();
        $flavor_category->id_user = Auth::id();
        $flavor_category->id_flavor = $this->flavor->id;
        $flavor_category->id_category = $categoria;
        $flavor_category->save();

    }
    return Redirect::back()->with('mensagem', 'Alterado com sucesso !')->with('flavor',$this->flavor->where('id_user',"=",Auth::id())->where('id','=',$req->id)->with('categories')->first())
        ->with('images',$this->image->where('id_flavor','=',$req->id)->get())->with('categorias',$this->category
            ->where('active','=','true')->where("id_user",'=',Auth::id())->get());



}

public function formUpdate(Request $req){
    return View::make('Flavor/formUpdate')->with('flavor',$this->flavor->where('id_user',"=",Auth::id())->where('id','=',$req->id)->with('categories')->first())
        ->with('images',$this->image->where('id_flavor','=',$req->id)->get())->with('categorias',$this->category
            ->where('active','=','true')->get());
}

    public function search(Request $req){
        if(!$req->nome){
            return View::make('Flavor/flavors')->with('flavors',$this->flavor->where("id_user",'=',Auth::id())->where("active",'=',true)->paginate(10));
        }
        return View::make('Flavor/flavors')->with('flavors',$this->flavor->where("id_user",'=',Auth::id())->where('name', 'ilike', "%$req->nome%")->where("active",'=',true)->paginate(10));
    }

    public function delete(Request $req){
        $flavor = $this->flavor->find($req->id);
        $flavor->active = false;
        $flavor->save();
        return Redirect::back()->with('mensagem', 'Removido com sucesso !');
    }

}
