<?php

namespace App\Http\Controllers;

use App\Drink;
use View;
use Redirect;
use Validator;
use Auth;
use Illuminate\Http\Request;

class DrinkController extends Controller
{
    protected $drink;
    function __construct(){
        $this->drink = New Drink();
    }
    public function drinks(){
        return View::make('Drink/drinks')->with('drinks',$this->drink->where('id_user','=',Auth::id())->where("active",'=',true)->orderBy('name','asc')->paginate(10));
    }
    public function formInsert(Request $req){
        return View::make('Drink/formInsert');
    }
    public function insert(Request $req){
        $req->preco = str_replace(",",".",$req->preco);
        $req->preco_promocional = str_replace(",",".",$req->preco_promocional);
        $validator = Validator::make($req->all(), $this->drink->validaCadastro());
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }
        $this->drink->name = $req->nome;
        $this->drink->price = $req->preco;
        $this->drink->price_promotional = $req->preco_promocional;
        $this->drink->id_user = Auth::id();
        $this->drink->active = true;
        $this->drink->save();
        return Redirect::back()->with('mensagem', 'Cadastrado com sucesso !');
    }
    public function list(Request $req){

       return response()->json($this->drink->where("id_user",'=',$req->id)->get());
    }

    public function update(Request $req){
        $req->preco = str_replace(",",".",$req->preco);
        $req->preco_promocional = str_replace(",",".",$req->preco_promocional);
        $validator = Validator::make($req->all(), $this->drink->validaAlterar());
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }
        $this->drink = $this->drink->find($req->id);
        $this->drink->name = $req->nome;
        $this->drink->price = $req->preco;
        $this->drink->promotion = $req->promocao;
        $this->drink->price_promotional = $req->preco_promocional;
        $this->drink->id_user = Auth::id();
        $this->drink->active = true;
        $this->drink->save();

        return Redirect::back()->with('mensagem', 'Alterado com sucesso !');
    }
    public function formUpdate(Request $req){
        return View::make('Drink/formUpdate')->with('drink',$this->drink->where('id_user',"=",Auth::id())->where('id','=',$req->id)->first());
    }
    public function search(Request $req){
        if(!$req->nome){
            return View::make('Drink/drinks')->with('drinks',$this->drink->where("id_user",'=',Auth::id())->where("active",'=',true)->paginate(10));
        }
        return View::make('Drink/drinks')->with('drinks',$this->drink->where("id_user",'=',Auth::id())->where('name', 'ilike', "%$req->nome%")->where("active",'=',true)->paginate(10));
    }
    public function delete(Request $req){
        $drink = $this->drink->find($req->id);
        $drink->active = false;
        $drink->save();
        return Redirect::back()->with('mensagem', 'Removido com sucesso !');
    }
}
