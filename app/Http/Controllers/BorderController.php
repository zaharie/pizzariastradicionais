<?php

namespace App\Http\Controllers;
use View;
use Redirect;
use Validator;
use Auth;
use App\Border;
use Illuminate\Http\Request;

class BorderController extends Controller
{

    protected $border;
    function __construct(){
        $this->border = New Border();
    }
    public function borders(){
        return View::make('Border/borders')->with('borders',$this->border->where('id_user','=',Auth::id())->where("active",'=',true)->orderBy('name','asc')->paginate(10));
    }
    public function list(Request $req){
        return response()->json($this->border->where('id_user','=',$req->id)->where('active','=','true')->get());
    }
    public function formInsert(Request $req){
        return View::make('Border/formInsert');
    }
    public function insert(Request $req){
        $validator = Validator::make($req->all(), $this->border->validaCadastro());
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }
        $this->border->name = $req->nome;
        $this->border->id_user = Auth::id();
        $this->border->active = true;
        $this->border->save();
        return Redirect::back()->with('mensagem', 'Cadastrado com sucesso !');
    }
    public function update(Request $req){
        $validator = Validator::make($req->all(), $this->border->validaCadastro());
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }
        $this->border = $this->border->find($req->id);
        $this->border->name = $req->nome;
        $this->border->id_user = Auth::id();
        $this->border->active = true;
        $this->border->save();
        return Redirect::back()->with('mensagem', 'Alterado com sucesso !');
    }

    public function formUpdate(Request $req){
        return View::make('Border/formUpdate')->with('border',$this->border->where('id_user',"=",Auth::id())->where('id','=',$req->id)->first());
    }

    public function search(Request $req){
        if(!$req->nome){
            return View::make('Border/borders')->with('borders',$this->border->where("id_user",'=',Auth::id())->where("active",'=',true)->paginate(10));
        }
        return View::make('Border/borders')->with('borders',$this->border->where("id_user",'=',Auth::id())->where('name', 'ilike', "%$req->nome%")->where("active",'=',true)->paginate(10));
    }

    public function delete(Request $req){
        $border = $this->border->find($req->id);
        $border->active = false;
        $border->save();
        return Redirect::back()->with('mensagem', 'Removido com sucesso !');
    }
}
