<?php

namespace App\Http\Controllers;
use View;
use Redirect;
use Auth;
use Validator;
use App\Housing;
use Illuminate\Http\Request;

class HousingController extends Controller
{
    protected $house;
    function __construct(){
        $this->house = New Housing();
    }
    public function houses(){
        return View::make('House/houses')->with('houses',$this->house->where('id_user','=',Auth::id())->where("active",'=',true)->orderBy('name','asc')->paginate(10));
    }
    public function formInsert(Request $req){
        return View::make('House/formInsert');
    }
    public function insert(Request $req){
        $req->taxa = str_replace(",",".",$req->taxa);
        $validator = Validator::make($req->all(), $this->house->validaCadastro());
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }
        $this->house->name = $req->nome;
        $this->house->tax = $req->taxa;
        $this->house->id_user = Auth::id();
        $this->house->active = true;
        $this->house->save();
        return Redirect::back()->with('mensagem', 'Cadastrado com sucesso !');
    }
    public function update(Request $req){
        $req->taxa = str_replace(",",".",$req->taxa);
        $validator = Validator::make($req->all(), $this->house->validaCadastro());
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }
        $this->house = $this->house->find($req->id);
        $this->house->name = $req->nome;
        $this->house->tax = $req->taxa;
        $this->house->id_user = Auth::id();
        $this->house->active = true;
        $this->house->save();
        return Redirect::back()->with('mensagem', 'Alterado com sucesso !');
    }
    public function formUpdate(Request $req){
        return View::make('House/formUpdate')->with('house',$this->house->where('id_user',"=",Auth::id())->where('id','=',$req->id)->first());
    }
    public function search(Request $req){
        if(!$req->nome){
            return View::make('House/houses')->with('houses',$this->house->where("id_user",'=',Auth::id())->where("active",'=',true)->paginate(10));
        }
        return View::make('House/houses')->with('houses',$this->house->where("id_user",'=',Auth::id())->where('name', 'ilike', "%$req->nome%")->where("active",'=',true)->paginate(10));
    }
    public function delete(Request $req){
        $house = $this->house->find($req->id);
        $house->active = false;
        $house->save();
        return Redirect::back()->with('mensagem', 'Removido com sucesso !');
    }
}
