<?php

namespace App\Http\Controllers;
use App\Payment;
use View;
use Redirect;
use Auth;
use Validator;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    protected  $payment;
    function __construct(){
        $this->payment = New Payment();
    }
    public function payments(){
        return View::make('Payment/payments')->with('payments',$this->payment->where('id_user','=',Auth::id())->where("active",'=',true)->orderBy('name','asc')->paginate(10));
    }
    public function formInsert(){
        return View::make('Payment/formInsert');
    }
    public function insert(Request $req){
        $validator = Validator::make($req->all(), $this->payment->validaCadastro());
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }
        $this->payment->name = $req->nome;
        $this->payment->id_user = Auth::id();
        $this->payment->active = true;
        $this->payment->save();
        return Redirect::back()->with('mensagem', 'Cadastrado com sucesso !');
    }
    public function update(Request $req){
        $validator = Validator::make($req->all(), $this->payment->validaCadastro());
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }
        $this->payment = $this->payment->find($req->id);
        $this->payment->name = $req->nome;
        $this->payment->id_user = Auth::id();
        $this->payment->active = true;
        $this->payment->save();
        return Redirect::back()->with('mensagem', 'Alterado com sucesso !');
    }
    public function formUpdate(Request $req){
        return View::make('Payment/formUpdate')->with('payment',$this->payment->where('id_user',"=",Auth::id())->where('id','=',$req->id)->first());
    }
    public function search(Request $req){
        if(!$req->nome){
            return View::make('Payment/payments')->with('payments',$this->payment->where("id_user",'=',Auth::id())->where("active",'=',true)->paginate(10));
        }
        return View::make('Payment/payments')->with('payments',$this->payment->where("id_user",'=',Auth::id())->where('name', 'ilike', "%$req->nome%")->where("active",'=',true)->paginate(10));
    }
    public function delete(Request $req){
        $payment = $this->payment->find($req->id);
        $payment->active = false;
        $payment->save();
        return Redirect::back()->with('mensagem', 'Removido com sucesso !');
    }
}
