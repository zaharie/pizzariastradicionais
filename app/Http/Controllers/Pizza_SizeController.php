<?php

namespace App\Http\Controllers;
use App\Pizza_size;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use View;
use Redirect;
use Validator;

class Pizza_sizeController extends Controller
{
    private $size;
    function __construct(){
        $this->size = new Pizza_size();
   }
    public function Sizes(){
     return View::make('Pizza_size/sizes')->with('sizes',$this->size->where("id_user",'=',Auth::id())->where("active",'=',true)->paginate(10));
    }
    public function formInsert(){
    return View::make('Pizza_size/formInsert');
}
    public function api(Request $req){

        return response()->json($this->size->where('id_user','=',$req->id)->get());
    }
    public function insert(Request $req){
        $req->preco = str_replace(",",".",$req->preco);
        $req->preco_promocional = str_replace(",",".",$req->preco_promocional);
        $req->price_border = str_replace(",",".",$req->price_border);
        $validator = Validator::make($req->all(), $this->size->validaCadastro());
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

          $this->size->id_user = Auth::id();
          $this->size->name = $req->nome;
          $this->size->price = $req->preco;
          $this->size->quantity_flavors = $req->quantidade_sabores;
          $this->size->price_promotional = $req->preco_promocional;
          $this->size->active = true;
          $this->size->price_border = $req->price_border;
          $this->size->quantity_pieces = $req->quantidade_pedacos;
          $this->size->save();
        return Redirect::back()->with('mensagem', 'Cadastrado com sucesso !');
    }
    public function update(Request $req){
        $req->preco = str_replace(",",".",$req->preco);
        $req->preco_promocional = str_replace(",",".",$req->preco_promocional);
        $req->price_border = str_replace(",",".",$req->price_border);
        $validator = Validator::make($req->all(), $this->size->validaCadastro());
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $size = $this->size->find($req->id);
        $size->name = $req->nome;
        $size->quantity_flavors = $req->quantidade_sabores;
        $size->price = $req->preco;
        $size->price_promotional = $req->preco_promocional;
        $size->promotion = $req->promocao;
        $size->price_border = $req->price_border;
        $size->active = true;
        $size->quantity_pieces = $req->quantidade_pedacos;
        $size->save();
        return Redirect::back()->with('mensagem', 'Alterado com sucesso !');
    }
    public function formUpdate(Request $req){
            return View::make("Pizza_size/formUpdate")->with('size', $this->size->where('id_user', '=', Auth::id())->where('id',$req->id)->first());
    }
    public function search(Request $req){
        if(!$req->nome){
            return View::make('Pizza_size/sizes')->with('sizes',$this->size->where("id_user",'=',Auth::id())->where("active",'=',true)->paginate(10));
        }
        return View::make('Pizza_size/sizes')->with('sizes',$this->size->where("id_user",'=',Auth::id())->where('name', 'ilike', "%$req->nome%")->where("active",'=',true)->paginate(10));
    }

    public function delete(Request $req){
        $size = $this->size->find($req->id);
        $size->active = false;
        $size->save();
        return Redirect::back()->with('mensagem', 'Removido com sucesso !');
    }
}
