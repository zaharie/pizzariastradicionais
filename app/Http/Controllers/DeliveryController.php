<?php

namespace App\Http\Controllers;
use App\Delivery;
use App\Neighborhood;
use Redirect;
use View;
use Validator;
use Auth;
use Illuminate\Http\Request;

class DeliveryController extends Controller
{
    protected  $delivery;
    function __construct(){
        $this->delivery = New Delivery();
    }
    public function formInsert(){

      if($this->delivery->where("id_user",'=', Auth::id())->first() !== null){

          return View::make('Delivery/formUpdate')->with("delivery",$this->delivery->where("id_user",'=',Auth::id())->first());
      }
        return View::make('Delivery/formInsert');
    }
    public function insert(Request $req){
        $req->taxa = str_replace(",",".",$req->taxa);
        $req->taxaMinima = str_replace(",",".",$req->taxaMinima);
        $this->delivery->tax_min = $req->taxaMinima;
        $this->delivery->tax = $req->taxa;
        $this->delivery->id_user = Auth::id();
        $this->delivery->save();
        return Redirect::back()->with('mensagem', 'Cadastrado com sucesso !');
    }
    public function update(Request $req){
        $req->taxa = str_replace(",",".",$req->taxa);
        $req->taxaMinima = str_replace(",",".",$req->taxaMinima);
        $delivery = $this->delivery->find($req->id);
        $delivery->tax_min = $req->taxaMinima;
        $delivery->tax = $req->taxa;
        $delivery->id_user = Auth::id();
        $delivery->save();
        return Redirect::back()->with('mensagem', 'Cadastrado com sucesso !');
    }

}
