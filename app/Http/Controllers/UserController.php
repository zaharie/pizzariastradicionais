<?php

namespace App\Http\Controllers;

use App\Order;
use App\User;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use View;
use Auth;
use Redirect;
class UserController extends Controller
{
    public $order;
    public $user;
   function  __construct(){
       $this->user = new User();
        $this->order = new Order();
}
    public function dash(){
       return View::make('Dash/user')->with("user",$this->user->where("id",'=',Auth::id())->first());
    }
    public function consumoHoje(){
        $now = Carbon::now();
    return View::make("Consumo.home")->with('order',$this->order->whereDay("created_at",'=',$now->day)->where("status",'!=',"Pendente")
        ->where('id_user','=',Auth::id())->get())->with('tempo',"Hoje");
}
    public function consumoMes(){
        $now = Carbon::now();

        return View::make("Consumo.home")->with('order',$this->order->whereYear('created_at', '=', $now->year)->where("status",'!=',"Pendente")
            ->where('id_user','=',Auth::id())->whereMonth('created_at', '=', $now->month)->get())->with('tempo',"Mês");;
    }

    public function consumoAno(){
        $now = Carbon::now();

        return View::make("Consumo.home")->with('order',$this->order->whereYear('created_at', '=', $now->year)->where("status",'!=',"Pendente") ->where('id_user','=',Auth::id())->get())->with('tempo',"Ano");
    }


    public function consumoTotal(){
        return View::make("Consumo.home")->with('order',$this->order->where('id_user','=',Auth::id())->where("status",'!=','Pendente')->get())->with('tempo',"Total");
    }




    public function alterar(Request $req){
        $validator = Validator::make($req->all(), $this->user->validaAlterar());
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

       $user = $this->user::find(Auth::id());
       $user->name = $req->name;
       $user->cep = $req->cep;
       $user->cnpj = $req->cnpj;
        $user->phone = $req->phone;
       $user->waiting_time = $req->waiting_time;
       $user->server_url = $req->server_url;
       $user->save();
       return Redirect::back()->with('mensagem', 'Removido com sucesso !');

    }

}
