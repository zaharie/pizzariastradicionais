<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Flavor_category extends Model
{
    public $table = "flavor_category";
    protected $primaryKey = ['id_flavor', 'id_category'];
    public $incrementing = false;


    public function validaCadastro()
    {
        return array('nome' => 'required|min:3','tipo' => 'required');
    }
}
