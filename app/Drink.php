<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Drink extends Model
{
    public $table = "drink";
    public function validaCadastro()
    {
        return array('nome' => 'required|min:3','preco' => 'required|numeric','preco_promocional' => 'required|numeric');
    }
    public function validaAlterar()
    {
        return array('nome' => 'required|min:3','preco' => 'required|numeric','preco_promocional' => 'required|numeric');
    }
}
