@extends('layouts.app')
@section('content')
</br>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <form>
                <div id="custom-search-input">
                    <div class="input-group col-md-12">
                        <input type="text" class="form-control input-lg" placeholder="Buscar" name="nome" />
                        <span class="input-group-btn">
                        <button class="btn btn-primary btn-lg" formaction="{{URL::route('saborBuscar')}}" type="submit">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </span>
                    </div>
                </div>
        </form>
        </div>
    </div>
    <table class="table table-striped">
        <hr>
        <a href={{URL::route('sabor')}}><span class="pull-right corMenu glyphicon glyphicon-plus"></span></a>
        <h3 class="branca">Lista de Sabores</h3>
        <hr>
        <thead>
        <tr>
            <th>Nome</th>
            <th>Preço</th>
            <th>Editar</th>
            <th>Excluir</th>

        </tr>
        </thead>
        <tbody>
        @foreach ($flavors as $flavor)
            <tr>
                <td>{{$flavor->name}}</td>
                @if($flavor->promotion == true)
                    <td class="bg-warning text-warning">{{"R$: ".str_replace('.',',',sprintf("%01.2f",$flavor->price_promotional))}}</td>
                    @else
                    <td>{{"R$: ".str_replace('.',',',sprintf("%01.2f",$flavor->price))}}</td>
                    @endif
                <td><button onclick="window.location='{{URL::route("saborAlterar",$flavor->id)}}'"><span class="glyphicon glyphicon-edit"></span></button></td>
                <td><button onclick="window.location='{{URL::route("saborDelete",$flavor->id)}}'"><span class="glyphicon glyphicon-remove"></span></button></td>
                @endforeach
            </tr>
        </tbody>
    </table>
    {{$flavors->links()}}
</div>
</div>
@endsection


