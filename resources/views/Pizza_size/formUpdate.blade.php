@extends('validation.validation')
@section('validation')
@endsection
@extends('layouts.app')
@section('content')
</br>
<div id="signupbox" style=" margin-top:50px" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
    <div class="panel panel-info">
        <div class="panel-heading">
            <div class="panel-title">Tamanho</div>
            <div style="float:right; font-size: 85%; position: relative; top:-10px"></div>
        </div>

        <div class="panel-body" >
            <form id="signupform" class="form-horizontal" enctype="multipart/form-data" role="form" id = "form" method="POST" action={{URL::route('PizzaTamanhoAlterar')}}>
                <div class="form-group">
                    <label for="firstname" class="col-md-3 control-label">Nome</label>
                    <div class="col-md-9 " id="nome">
                        <input type="text" class="form-control"  value="{{$size['name']}}" name="nome" placeholder="Nome">
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-md-3 control-label">Preço</label>
                    <div class="col-md-9" id="preco">
                        <input type="text" class="form-control" value="{{str_replace(',','.',sprintf("%01.2f",$size['price']))}}" onblur="replace()" name="preco" placeholder="Preço">
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-md-3 control-label">Preço Promocional</label>
                    <div class="col-md-9" id="preco_promocional">
                        <input type="text" class="form-control" onblur="replace()" value="{{str_replace(',','.',sprintf("%01.2f",$size['price_promotional']))}}" name="preco_promocional" placeholder="Preço Promocional">
                    </div>
                </div>
                <div class="form-group">
                    <label for="price_border" class="col-md-3 control-label">Preço Borda</label>
                    <div class="col-md-9" id="price_border">
                        <input type="text" class="form-control" onblur="replace()" value="{{str_replace(',','.',sprintf("%01.2f",$size['price_border']))}}" name="price_border" placeholder="Preço Borda">
                    </div>
                </div>
                <div class="form-group">
                    <label for="promocao" class="col-md-3 control-label">Em Promocao</label>
                    <div class="col-md-9" id="promocao">
                        @if($size['promotion'] == true)
                            <input type="hidden" name="promocao" value="0">
                            <input type="checkbox" name="promocao" value="1" checked>
                        @else
                            <input type="hidden" name="promocao" value="0">
                            <input type="checkbox" name="promocao" value="1">
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label for="quantidade_pedacos" class="col-md-3 control-label">Quantidade de pedaços</label>
                    <div class="col-md-9" id="quantidade_pedacos">
                        <input type="number" class="form-control" value="{{$size['quantity_pieces']}}" name="quantidade_pedacos" placeholder="Quantidade de pedaços">
                    </div>
                </div>
                <div class="form-group">
                    <label for="quantidade_sabores" class="col-md-3 control-label">Quantidade de sabores</label>
                    <div class="col-md-9" id="quantidade_sabores">
                        <input type="number" class="form-control" value="{{$size['quantity_flavors']}}" name="quantidade_sabores" placeholder="Quantidade de sabores">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-3 col-md-9">
                        <button id="btn-signup" type="submit" class="btn  btn-primary btn-block"><i class="icon-hand-right"></i>Cadastrar</button>
                    </div>
                </div>
                <input type="hidden" name="id" value={{$size['id']}}>
                {{ csrf_field()}}
            </form>
        </div>

    </div>
    <a href='{{URL::route('pizzaTamahos')}}'><span class="pull-left btn btn-primary corMenuBackground">Voltar</span></a>
</div>
</div>
</div>
<button type="button" data-toggle="modal" id="sucesso" data-target="#myModal"></button>
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Mensagem:</h4>
            </div>
            <div class="modal-body">
                <p>{{Session::get('mensagem')}}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
</div>
<script>
    function replace() {
        var valor =  $("#preco>input").val();
        $("#preco>input").val(valor.replace(",",'.'));
        var valor =  $("#preco_promocional>input").val();
        $("#preco_promocional>input").val(valor.replace(",",'.'));
        var valor =  $("#price_border>input").val();
        $("#price_border>input").val(valor.replace(",",'.'));
    }
</script>
@if(Session::get('mensagem'))
    <script type="text/javascript">
        $(document).ready(function () {
            $("#sucesso").click();
        })
    </script>
    @endif
@endsection

