@extends('validation.validation')
@section('validation')
@endsection
@extends('layouts.app')
@section('content')
</br>
<div id="signupbox" style=" margin-top:50px" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
    <div class="panel panel-info">
        <div class="panel-heading">
            <div class="panel-title">Taxa de entrega</div>
            <div style="float:right; font-house: 85%; position: relative; top:-10px"></div>
        </div>
        <div class="panel-body" >
            <form id="signupform" class="form-horizontal" enctype="multipart/form-data" role="form" id = "form" method="POST" action={{URL::route('deliveryUpdate')}}>
                <div class="form-group">
                    <label for="taxaMinima" class="col-md-3 control-label">Taxa Minima</label>
                    <div class="col-md-9 " id="taxaMinima">
                        <input type="text" class="form-control"  value="{{$delivery['tax_min']}}" name="taxaMinima" placeholder="Taxa Minima">
                    </div>
                </div>
                <div class="form-group">
                    <label for="taxa" class="col-md-3 control-label">Taxa</label>
                    <div class="col-md-9" id="taxa">
                        <input type="text" class="form-control"  value="{{str_replace(',','.',sprintf("%01.2f",$delivery['tax']))}}" onblur="replace()" name="taxa" placeholder="Taxa">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-3 col-md-9">
                        <button id="btn-signup" type="submit" class="btn  btn-primary btn-block"><i class="icon-hand-right"></i>Alterar</button>
                    </div>
                </div>
                <input type="hidden" name="id" value={{$delivery['id']}}>
                {{ csrf_field()}}
            </form>
        </div>

    </div>
    <a href='{{URL::route('home')}}'><span class="pull-left btn btn-primary corMenuBackground">Voltar</span></a>
</div>
</div>
</div>
<button type="button" data-toggle="modal" hidden id="sucesso" data-target="#myModal"></button>
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Mensagem:</h4>
            </div>
            <div class="modal-body">
                <p>{{Session::get('mensagem')}}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
</div>
<script>
    function replace() {
        var valor =  $("#taxa>input").val();
        $("#taxa>input").val(valor.replace(",",'.'));
        var valor =  $("#taxaMinima>input").val();
        $("#taxaMinima>input").val(valor.replace(",",'.'));
    }
</script>
@if(Session::get('mensagem'))
    <script type="text/javascript">
        $(document).ready(function () {
            $("#sucesso").click();
        })
    </script>
@endif
@endsection