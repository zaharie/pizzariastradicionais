@extends('validation.validation')
@section('validation')
@endsection
@extends('layouts.app')
@section('content')
</br>
<div id="signupbox" style=" margin-top:50px" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
    <div class="panel panel-info">
        <div class="panel-heading">
            <div class="panel-title">Ingrediente</div>
            <div style="float:right; font-border: 85%; position: relative; top:-10px"></div>
        </div>

        <div class="panel-body" >
            <form id="signupform" class="form-horizontal" enctype="multipart/form-data" role="form" id = "form" method="POST" action={{URL::route('categoriaPostAlterar')}}>
                <div class="form-group">
                    <label for="firstname" class="col-md-3 control-label">Nome</label>
                    <div class="col-md-9 " id="nome">
                        <input type="text" class="form-control"  value="{{$category['name']}}" name="nome" placeholder="Nome">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-3 col-md-9">
                        <button id="btn-signup" type="submit" class="btn  btn-primary btn-block"><i class="icon-hand-right"></i>Alterar</button>
                    </div>
                </div>
                <input type="hidden" name="id" value={{$category['id']}}>
                {{ csrf_field()}}
            </form>
        </div>

    </div>
    <a href='{{URL::route('categorias')}}'><span class="pull-left btn btn-primary corMenuBackground">Voltar</span></a>
</div>
</div>
</div>
<button type="button" data-toggle="modal" hidden id="sucesso" data-target="#myModal"></button>
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Mensagem:</h4>
            </div>
            <div class="modal-body">
                <p>{{Session::get('mensagem')}}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
</div>
<script>
    function replace() {
        var valor =  $("#preco>input").val();
        $("#preco>input").val(valor.replace(",",'.'));
        var valor =  $("#preco_promocional>input").val();
        $("#preco_promocional>input").val(valor.replace(",",'.'));
    }
</script>
@if(Session::get('mensagem'))
<script type="text/javascript">
    $(document).ready(function () {
        $("#sucesso").click();
    })
</script>
@endif
@endsection