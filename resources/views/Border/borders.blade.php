@extends('layouts.app')
@section('content')
</br>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <form>
                <div id="custom-search-input">
                    <div class="input-group col-md-12">
                        <input type="text" class="form-control input-lg" placeholder="Buscar" name="nome" />
                        <span class="input-group-btn">
                        <button class="btn btn-primary btn-lg" formaction="{{URL::route('bordaBuscar')}}" type="submit">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </span>
                    </div>
                </div>
        </form>
        </div>
    </div>
    <table class="table table-striped">
        <hr>
        <a href={{URL::route('borda')}}><span class="pull-right corMenu glyphicon glyphicon-plus"></span></a>
        <h3 class="branca">Lista de Bordas</h3>
        <hr>
        <thead>
        <tr>
            <th>Nome</th>
            <th>Editar</th>
            <th>Excluir</th>

        </tr>
        </thead>
        <tbody>
        @foreach ($borders as $border)
            <tr>
                <td>{{$border->name}}</td>
                <td><button onclick="window.location='{{URL::route("bordaAlterar",$border->id)}}'"><span class="glyphicon glyphicon-edit"></span></button></td>
                <td><button onclick="window.location='{{URL::route("bordaDelete",$border->id)}}'"><span class="glyphicon glyphicon-remove"></span></button></td>
                @endforeach
            </tr>
        </tbody>
    </table>
    {{$borders->links()}}
</div>
</div>
@endsection


