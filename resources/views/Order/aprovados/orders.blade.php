@extends('layouts.app')
@section('content')
</br>
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.3/socket.io.js"></script>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <form>
                <div id="custom-search-input">
                    <div class="input-group col-md-12">
                        <input type="text" class="form-control input-lg" placeholder="Buscar" name="id" />
                        <span class="input-group-btn">
                               <button class="btn btn-primary btn-lg" formaction="{{URL::route('pedidoBuscarAprovados')}}" type="submit">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </span>
                    </div>
                </div>
        </form>
        </div>
    </div>
    <button onclick="window.location='{{URL::route("pedidosFinalizar")}}'" style="margin-top: 3%" class="glyphicon glyphicon-edit pull-right"></button>
    <table class="table table-striped">
        <hr>
        <h3 class="branca">Pedidos Aprovados</h3>
        <hr>
        <thead>
        <tr>
            <th>Numero do pedido</th>
            <th>Status</th>
            <th>Detalhes</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach ($orders as $order)
            <tr>
                <td>{{$order->order_id}}</td>
                <td>{{$order->status}}</td>
                <td><button onclick="window.location='{{URL::route("pedidoAprovadoDetalhes",$order->id)}}'"><span class="glyphicon glyphicon-edit"></span></button></td>
                @endforeach
            </tr>
        </tbody>
    </table>
    {{$orders->links()}}
</div>
</div>
@endsection

