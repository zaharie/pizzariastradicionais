@extends('layouts.app')
@section('content')
</br>
<div class="container">
    <div class="row pedido">
        <h4 class="text-center">Pedido {{$pedido->order_id}}</h4>
        <div class="col-md-3">
            <div class="">

                <p class="">Nome: {{$client->name}} {{$client->last_name}}</p>
                <img class="img-thumbnail img-circle" width="125px" src="{{$client->profile_pic}}">
                <p>Cep: {{$pedido->zip_code}}</p>
                @if(!property_exists ($endereco,"erro"))
                    <p>{{$endereco->logradouro}}</p>
                    <p>Bairro: {{$endereco->bairro}}</p>
                    @endif
                <p>Complemento: {{$pedido->complement}}</p>
                <p>Moradia : {{$pedido->housing}}</p>
                <p>Numero : {{$pedido->number}}</p>
                <p>Telefone : {{$pedido->phone}}</p>
                <p>Metodo de pagamento : {{$pedido->payment_method}}</p>
                @if($pedido->change !== null)
                    <p>Troco para R$ : {{$pedido->change}}</p>
                @endif
            </div>
    </div>
        <div class="col-md-9">
            <hr>

        @foreach ($pizzas as $pizza)
            <p>Item: Pizza</p>
                <p>Borda: {{$pizza->borda}} {{"R$: ".str_replace('.',',',sprintf("%01.2f",$pizza->border_price))}}</p>

            <p>Tamanho : {{$pizza->size}}</p>
            <span class="sabores">Sabores: /</span>
            @foreach($pizza->sabores as $sabor)
                    <span class="sabores">{{$sabor}} /</span>
            @endforeach
                @php

                  $pizzaTotal = (array_sum($pizza->sabores_price) + $pizza->size_price* count($pizza->sabores_price))/ count($pizza->sabores_price) + $pizza->border_price;
                  $pizzaTotal = $pizzaTotal * $pizza->quantity;
                @endphp
                <p>Quantidade : {{$pizza->quantity}}</p>
                <p>Observação : {{$pizza->obs}}</p>
                <p>Valor: {{"R$: ".str_replace('.',',',sprintf("%01.2f",$pizzaTotal))}}</p>
                <hr>
        @endforeach
            @foreach ($drinks as $drink)
                <p>{{$drink->name}}</p>
                <p>Quantidade: {{$drink->quantity}}</p>
                <p class=""> Valor {{"R$: ".str_replace('.',',',sprintf("%01.2f",$drink->price * $drink->quantity))}}</p>
        <hr>
            @endforeach
            <p class="">Taxa de {{$taxaMoradia->name}} {{"R$: ".str_replace('.',',',sprintf("%01.2f",$taxaMoradia->tax))}}</p>
            <hr>
            <p class="">Taxa de Entrega {{"R$: ".str_replace('.',',',sprintf("%01.2f",$pedido->delivery_tax))}}</p>
            <hr>
    </div>
        <div class="col-xs-12" style="margin-bottom: 2%">
        <p class="col-md-12 text-right">Total: {{"R$: ".str_replace('.',',',sprintf("%01.2f",$pedido->total))}}</p>
        </div>
        <div class="col-xs-12" hidden>
          <button class="btn btn-primary pull-right">Imprimir</button>
        </div>
    </div>

    <div class="col-xs-12" style="margin-top: 2%;margin-bottom: 2%">
        <div class="col-xs-1 pull-left ">
            <button onclick="window.location='{{URL::route("pedidos")}}'" class="btn pull-right btn-primary">Voltar</button>
        </div>
        <div class="col-xs-1 col-xs-offset-8 ">
            <button onclick="aprovar()" class="btn pull-right btn-primary">Aprovar</button>
        </div>
        <div class="col-xs-1">
            <button onclick="window.open('{{URL::route("imprimirPedido",$pedido->id)}}')" class="btn pull-right btn-primary">Imprimir</button>
        </div>
        <div class="col-xs-1">
            <button onclick="recusar()" class="btn pull-right btn-primary">Recusar</button>
        </div>
    </div>
</div>
@endsection

<script>
    function aprovar() {
        swal({
            title: 'Tem certeza ?',
            text: "Verifique o pedido direitinho antes de aprovar !",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Confirmar'
        }).then((result) => {
            if (result.value) {
            swal(
                'Confirmado !',
                'O pedido foi confirmado',
                'success'
            )
            window.location='{{URL::route("pedidoAprovar",$pedido->id)}}'
        }
    });
    }


    async function  recusar(){
        const {value: text} = await swal({
            input: 'textarea',
            inputPlaceholder: 'Digite o motivo',
            showCancelButton: true
        });

        if (text) {
            window.location='{{URL::route("pedidoRecusar",$pedido->id)}}'+"/"+text;
            console.log(text);
        }

    }


</script>