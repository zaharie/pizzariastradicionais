@extends('layouts.app')
@section('content')
</br>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <form>
                <div id="custom-search-input">
                    <div class="input-group col-md-12">
                        <input type="text" class="form-control input-lg" placeholder="Buscar" name="nome" />
                        <span class="input-group-btn">
                        <button class="btn btn-primary btn-lg" formaction="{{URL::route('pagamentoBuscar')}}" type="submit">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </span>
                    </div>
                </div>
        </form>
        </div>
    </div>
    <table class="table table-striped">
        <hr>
        <a href={{URL::route('pagamento')}}><span class="pull-right corMenu glyphicon glyphicon-plus"></span></a>
        <h3 class="branca">Lista de Metodos de Pagamentos</h3>
        <hr>
        <thead>
        <tr>
            <th>Nome</th>
            <th></th>
            <th>Editar</th>

            <th>Excluir</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($payments as $payment)
            <tr>
                <td>{{$payment->name}}</td>
                <td></td>
                <td><button onclick="window.location='{{URL::route("pagamentoAlterar",$payment->id)}}'"><span class="glyphicon glyphicon-edit"></span></button></td>
                <td><button onclick="window.location='{{URL::route("pagamentoDelete",$payment->id)}}'"><span class="glyphicon glyphicon-remove"></span></button></td>
                @endforeach
            </tr>
        </tbody>
    </table>
    {{$payments->links()}}
</div>
</div>
@endsection


