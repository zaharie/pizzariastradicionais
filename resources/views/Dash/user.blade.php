@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Dash</div>

                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('alterar') }}">
                            {{ csrf_field() }}
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Nome</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name"  value="{{ $user->name }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('cep') ? ' has-error' : '' }}">
                                <label for="cep" class="col-md-4 control-label">Cep</label>
                                <div class="col-md-6">
                                    <input id="cep" type="text" class="form-control" name="cep" value="{{ $user->cep }}" required >
                                    @if ($errors->has('cep'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('cep') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>



                            <div class="form-group{{ $errors->has('server_url') ? ' has-error' : '' }}">
                                <label for="cep" class="col-md-4 control-label">Cep</label>
                                <div class="col-md-6">
                                    <input id="cep" type="text" class="form-control" name="server_url" value="{{ $user->server_url }}" required >
                                    @if ($errors->has('server_url'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('server_url') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                <label for="phone" class="col-md-4 control-label">Phone</label>
                                <div class="col-md-6">
                                    <input id="phone" type="text" class="form-control" name="phone" value="{{ $user->phone }}" required >
                                    @if ($errors->has('phone'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('cnpj') ? ' has-error' : '' }}">
                                <label for="cnpj" class="col-md-4 control-label">Cnpj</label>

                                <div class="col-md-6">
                                    <input id="cnpj" type="text" class="form-control" name="cnpj" value="{{ $user->cnpj }}" required >
                                    @if ($errors->has('cnpj'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('cnpj') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('waiting_time') ? ' has-error' : '' }}">
                                <label for="waiting_time" class="col-md-4 control-label">Tempo medio de espera</label>

                                <div class="col-md-6">
                                    <input id="waiting_time" type="number" class="form-control" name="waiting_time" value="{{ $user->waiting_time }}" required>
                                    @if ($errors->has('waiting_time'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('waiting_time') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Alterar
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
