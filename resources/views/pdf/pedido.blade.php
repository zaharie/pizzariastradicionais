
<html>
<head>
    <meta charset="UTF-8">
</head>
<body>
<style>
    body,html {
    height: 100%;
    }
    *{
        font-family: Arial, Helvetica, sans-serif;
        font-size: 50px;
        font-weight: bolder;
    }
    .grande{
    font-size: 80px;
    }

</style>
<div class="col-md-9">
    <h1 style="text-align: center">Pedido {{$pedido->order_id}}</h1>
    <hr>
    <p>Nome do cliente : {{$client->name}} {{$client->last_name}}</p>
    <p>{{$endereco->logradouro}}</p>
    <p>Bairro: {{$endereco->bairro}}</p>
    <p>Cep: {{$endereco->cep}}</p>
    <p>Tipo de residencia : {{$pedido->housing}}</p>
    <p>Numero : {{$pedido->number}}</p>
    <p>Complemento: {{$pedido->complement}}</p>
    <p>Telefone : {{$pedido->phone}}</p>
    <p>Metodo de pagamento : {{$pedido->payment_method}}</p>
    @if($pedido->change !== null)
        <p>Troco para R$ : {{$pedido->change}}</p>
    @endif

    <hr>
    @foreach ($pizzas as $pizza)
        <p class="grande">Item: Pizza</p>
    @if($pizza->borda)
        <p class="grande">Borda: {{$pizza->borda}}
        @endif
        <p class="grande">Tamanho : {{$pizza->size}}</p>
        <span class="sabores grande">Sabores: /</span>
        @foreach($pizza->sabores as $sabor)
            <span class="sabores grande">{{$sabor}} /</span>
        @endforeach
        @php
            $pizzaTotal = (array_sum($pizza->sabores_price) + $pizza->size_price* count($pizza->sabores_price))/ count($pizza->sabores_price) + $pizza->border_price;
            $pizzaTotal = $pizzaTotal * $pizza->quantity;
        @endphp
        <p class="grande">Quantidade : {{$pizza->quantity}}</p>
    @if($pizza->obs)
        <p class="grande">Observação : {{$pizza->obs}}</p>
        @endif
        <p>Valor: {{"R$: ".str_replace('.',',',sprintf("%01.2f",$pizzaTotal))}}</p>
        <hr>
    @endforeach
    @foreach ($drinks as $drink)
        <p>Item: {{$drink->name}}</p>
        <p>Quantidade: {{$drink->quantity}}</p>
        <p class=""> Valor {{"R$: ".str_replace('.',',',sprintf("%01.2f",$drink->price * $drink->quantity))}}</p>
        <hr>
    @endforeach
    <p class="">Taxa de Entrega {{"R$: ".str_replace('.',',',sprintf("%01.2f",$pedido->delivery_tax + $taxaMoradia->tax))}}</p>
    <hr>
</div>
<div class="col-xs-12" style="margin-bottom: 2%">
    <p class="col-md-12 text-right">Total: {{"R$: ".str_replace('.',',',sprintf("%01.2f",$pedido->total))}}</p>
</div>
</div>
</body>
</html>