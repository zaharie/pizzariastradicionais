@yield('validation')
<script src="{{ asset('js/jquery.js') }}"></script>
@if($errors->has('nome'))
    <script type="text/javascript">
        $("#nome").append("<p class = 'alert-danger'>"+"{{$errors->first('nome')}}"+"<p>");
    </script>
@endif
@if($errors->has('preco'))
    <script type="text/javascript">
        $("#preco").append("<p class = 'alert-danger'>"+"{{$errors->first('preco')}}"+"<p>");
    </script>
@endif
@if($errors->has('quantidade_pedacos'))
    <script type="text/javascript">
        $("#quantidade_pedacos").append("<p class = 'alert-danger'>"+"{{$errors->first('quantidade_pedacos')}}"+"<p>");
    </script>
@endif
@if($errors->has('img'))
    <script type="text/javascript">
        $("#img").append("<p class = 'alert-danger'>"+"{{$errors->first('img')}}"+"<p>");
    </script>
@endif
@if($errors->has('desc'))
    <script type="text/javascript">
        $("#desc").append("<p class = 'alert-danger'>"+"{{$errors->first('desc')}}"+"<p>");
    </script>
@endif
@if($errors->has('preco_promocional'))
    <script type="text/javascript">
        $("#preco_promocional").append("<p class = 'alert-danger'>"+"{{$errors->first('preco_promocional')}}"+"<p>");
    </script>
@endif
@if($errors->has('taxa'))
    <script type="text/javascript">
        $("#taxa").append("<p class = 'alert-danger'>"+"{{$errors->first('taxa')}}"+"<p>");
    </script>
@endif
@if($errors->has('quantidade_sabores'))
    <script type="text/javascript">
        $("#quantidade_sabores").append("<p class = 'alert-danger'>"+"{{$errors->first('quantidade_sabores')}}"+"<p>");
    </script>
@endif
@if($errors->has('categoria'))
    <script type="text/javascript">
        $("#categoria").append("<p class = 'alert-danger'>"+"{{$errors->first('categoria')}}"+"<p>");
    </script>
@endif
    @if($errors->has('price_border'))
        <script type="text/javascript">
            $("#price_border").append("<p class = 'alert-danger'>"+"{{$errors->first('price_border')}}"+"<p>");
        </script>
    @endif