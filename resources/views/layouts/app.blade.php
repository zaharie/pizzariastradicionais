<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Enfoca Hr</title>

    <link rel="icon" type="image/png" sizes="16x16" href="{{asset("favicon/favicon-16x16.png")}}">
    <script type="text/javascript"   src="https://code.jquery.com/jquery-3.2.1.js"></script>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>
    <script type="text/javascript" src="http://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
    <script type="text/javascript" src="{{asset("js/jquery.tokeninput.js")}}" ></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://unpkg.com/sweetalert2@7.3.2/dist/sweetalert2.all.js" type="text/javascript"></script>




    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/token-input.css') }}" rel="stylesheet">
    <link href="{{ asset('css/token-input-facebook.css') }}" rel="stylesheet">
    <link href="{{ asset('css/css.css') }}" rel="stylesheet">



</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top" style="background-color:#3097D1;">
            <div class="container">

                @if (Auth::check())
                <div class="navbar-wrapper">
                    <div class="container-fluid">
                        <nav class="navbar navbar-header">
                            <div class="container">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                    <a class="navbar-brand white" href="#">Enfoca</a>
                                </div>
                                <div id="navbar" class="navbar-collapse collapse">
                                    <ul class="nav navbar-nav">
                                        <li><a href="{{URL::route('home')}}" class="">Home</a></li>
                                        <li class=" dropdown">
                                            <a href="#" class="white dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Pizza<span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="{{URL::route('sabores')}}">Sabores</a></li>
                                                <li><a href="{{URL::route('bordas')}}">Bordas</a></li>
                                                <li><a href="{{URL::route('pizzaTamahos')}}">Tamanhos</a></li>
                                                <li><a href="{{URL::route('categorias')}}">Ingredientes</a></li>
                                            </ul>
                                        </li>
                                        <li class=" dropdown">
                                            <a href="#" class="dropdown-toggle " data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Produtos<span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="{{URL::route('bebidas')}}">Bebidas</a></li>
                                            </ul>
                                        </li>
                                        <li class=" dropdown">
                                            <a href="#" class="dropdown-toggle " data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Taxas<span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                                <li class=" dropdown">
                                                <li><a href="{{URL::route('moradias')}}">Moradias</a></li>
                                                </li>
                                                <li><a href="{{URL::route('delivery')}}">Delivery</a></li>
                                            </ul>
                                        </li>
                                        <li class=" dropdown">
                                            <a href="#" class="dropdown-toggle " data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Pedidos<span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                                <li class=" dropdown">
                                                <li><a href="{{URL::route('pedidos')}}">Pedidos Pendentes</a></li>
                                                </li>
                                                <li><a href="{{URL::route('pedidosAprovados')}}">Pedidos Aprovados</a></li>
                                                <li><a href="{{URL::route('pedidosFinalizados')}}">Pedidos Finalizados</a></li>
                                            </ul>
                                        </li>
                                        <li class=" dropdown">
                                            <a href="#" class="dropdown-toggle " data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dados<span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="{{URL::route('consumo')}}">Consumo</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                    @endif
                                    <ul class="nav navbar-nav pull-right">

                                        @if (Auth::guest())
                                            <li><a href="{{ route('login') }}">Login</a></li>
                                            <li><a href="{{ route('register') }}">Register</a></li>
                                        @else
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                                    {{ Auth::user()->name }} <span class="caret"></span>
                                                </a>

                                                <ul class="dropdown-menu" role="menu">
                                                    <li>
                                                        <a href="{{ route('dash')}}">Conta</a>
                                                    </li>
                                                    <li>
                                                        <a href="{{ route('logout') }}"
                                                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                            Logout
                                                        </a>

                                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                            {{ csrf_field() }}
                                                        </form>
                                                    </li>

                                                </ul>
                                            </li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </nav>
                    </div>
                </div>





            </div>
        </nav>

       <section class=container-fluid>
           @yield('content')
       </section>
    </div>

</body>

</html>
