@extends('layouts.app')
@section('content')
<div class="container" style="background-color: white">
    <div class="row">
        <h3 class="text-center">Dados de consumo {{$tempo}}</h3>
    </div>
    <div class="row">
        <button class="w3-button w3-white w3-border w3-border-blue" onclick="window.location='{{URL::route("consumo")}}'">Hoje</button>
        <button class="w3-button w3-white w3-border w3-border-blue"  onclick="window.location='{{URL::route("consumoMes")}}'">Mês</button>
        <button class="w3-button w3-white w3-border w3-border-blue"  onclick="window.location='{{URL::route("consumoAno")}}'">Ano</button>
        <button class="w3-button w3-white w3-border w3-border-blue" onclick="window.location='{{URL::route("consumoTotal")}}'" >Total</button>
        <div class="col-md-12 col-lg-12 col-xs-12">
        <h4>Numero de pedidos {{count($order)}}</h4>
            <h4>Taxa de atendimento por pedido R$: 0,90</h4>
            <h5 class="text-right alert-info">Total estimado de R$: {{str_replace('.',',',sprintf("%01.2f",count($order)* 0.90))}}</h5>
        </div>
    </div>
</div>
@endsection
